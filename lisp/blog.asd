(defsystem "blog"
  :version "0.1.0"
  :author ""
  :license ""
  :depends-on ()
  :components ((:module "src"
                :components
                ((:file "packages")
                 (:file "000"))))
  :description ""
  :in-order-to ((test-op (test-op "blog/tests"))))

(defsystem "blog/tests"
  :author ""
  :license ""
  :depends-on ("blog"
               "rove")
  :components ((:module "tests"
                :components
                ((:file "main"))))
  :description "Test system for blog"
  :perform (test-op (op c) (symbol-call :rove :run c)))
