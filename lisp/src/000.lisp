(in-package :blog)


;;; utils

(defun min-frame-width (number base)
  "The minimum string size needed to print the number in the base."
  (if (zerop number)
      1
      (ceiling (log (1+ number) base))))


;;; hex

(defvar *print-hex-case* :lower)

(defun hex-char-p (char)
  "A hex char is one of 0-9, A-F, or a-f."
  (let ((code (char-code char))
        (lower-a-code (char-code #\a))
        (lower-f-code (char-code #\f))
        (upper-a-code (char-code #\A))
        (upper-f-code (char-code #\F))
        (0-code (char-code #\0))
        (9-code (char-code #\9)))
    (or (and (>= code lower-a-code)
             (<= code lower-f-code))
        (and (>= code upper-a-code)
             (<= code upper-f-code))
        (and (>= code 0-code)
             (<= code 9-code)))))

(defun hex-code-p (code)
  "A hex-code is an integer between 0 and 15 inclusive."
  (and (typep code 'integer)
       (>= code 0)
       (<= code 15)))

(deftype hex-char ()
  "A hex-char is one of 0-9, A-F, or a-f."
  `(satisfies hex-char-p))

(deftype hex-code ()
  "A hex-code is an integer between 0 and 15 inclusive."
  `(satisfies hex-code-p))

(defun hex-char (code)
  "Return the hex char for the code where 0->0 and 15->f. If the code is above 9, and *print-hex-case*
  is :upper, return an upper-case char, otherwise the default is :lower to return a lower-case char."
  (assert (typep code 'hex-code))
  (code-char (if (<= code 9)
                 (+ code (char-code #\0))
                 (+ code -10 (cond ((eq *print-hex-case* :lower)
                                    (char-code #\a))
                                   ((eq *print-hex-case* :upper)
                                    (char-code #\A))
                                   (t (error "Invalid print-hex-case: ~A" *print-hex-case*)))))))


(defun char-hex (char)
  "Return the 4-bit hex integer represented by the character."
  (assert (typep char 'hex-char))
  (let* ((code (char-code char))
         (lower-a-code (char-code #\a))
         (lower-f-code (char-code #\f))
         (upper-a-code (char-code #\A))
         (upper-f-code (char-code #\F))
         (0-code (char-code #\0))
         (9-code (char-code #\9)))
    (cond ((and (>= code 0-code)
                (<= code 9-code))
           (- code 0-code))
          ((and (>= code lower-a-code)
                (<= code lower-f-code))
           (- code -10 lower-a-code))
       ((and (>= code upper-a-code)
             (<= code upper-f-code))
        (- code -10 upper-a-code)))))

(defun write-hex (number &key stream frame-width)
  "Write the number, in hex, to the place. Typical places will be streams and strings."
  (let ((frame-width (if frame-width frame-width
                         (min-frame-width number 16))))
    (loop for index from (* (1- frame-width) 4) downto 0 by 4
          do (write-char
               (hex-char (ldb (byte 4 index)
                              number))
               stream)))
  number)

(defun print-hex (number &optional (stream *standard-output*) frame-width)
  "Print the number in hexidecimal. Print to the stream if given, otherwise
  return a string."
  (if stream
      (progn
        (fresh-line stream)
        (write-hex number :stream stream :frame-width frame-width))
      (with-output-to-string (string)
        (write-hex number :stream string :frame-width frame-width))))


;;; b64

(defun b64-char-p (char)
  "A b64 char is one of 0-9, A-Z, a-z. Or one of #\\ #\+ #\=."
  (let ((code (char-code char))
        (lower-a-code (char-code #\a))
        (lower-z-code (char-code #\z))
        (upper-a-code (char-code #\A))
        (upper-Z-code (char-code #\Z))
        (0-code (char-code #\0))
        (9-code (char-code #\9))
        (/-code (char-code #\/))
        (+-code (char-code #\+)))
    (or (and (>= code lower-a-code)
             (<= code lower-z-code))
        (and (>= code upper-a-code)
             (<= code upper-z-code))
        (and (>= code 0-code)
             (<= code 9-code))
        (= code /-code)
        (= code +-code))))

(defun b64-code-p (code)
  "A b64-code is an integer between 0 and 63 inclusive."
  (typep code '(integer 0 63)))

(defun b64-pad-p (char)
  "A b64-pad is the = character."
  (char= char #\=))

(deftype b64-char ()
  "A b64 char is one of 0-9, A-Z, a-z. Or one of #\\ #\+ #\=."
  `(satisfies b64-char-p))

(deftype b64-code ()
  "A b64-code is an integer between 0 and 63 inclusive."
  `(satisfies b64-code-p))

(deftype b64-pad ()
  "A b64-pad is the = character."
  `(satisfies b64-pad-p))

(defun b64-char (b64-code)
  "Return the b64 char for the 6-bit code."
  (assert (typep b64-code 'b64-code))
  (let ((code (cond ((<= b64-code 25)
                     (+ b64-code (char-code #\A)))
                    ((<= b64-code 51)
                     (+ -26 b64-code (char-code #\a)))
                    ((<= b64-code 61)
                     (+ -52 b64-code (char-code #\0)))
                    ((= b64-code 62)
                     (char-code #\+))
                    ((= b64-code 63)
                     (char-code #\/))
                    (t (error "Not a b64-code")))))
    (code-char code)))

(defun char-b64 (char)
 "Return the 6-bit b64 code represented by the b64 character."
 (assert (typep char 'b64-char))
 (let ((upper-case-a-code (char-code #\A))
       (upper-case-z-code (char-code #\Z))
       (lower-case-a-code (char-code #\a))
       (lower-case-z-code (char-code #\z))
       (zero-code (char-code #\0))
       (nine-code (char-code #\9))
       (code (char-code char)))
   (cond ((and (>= code upper-case-a-code)
               (<= code upper-case-z-code))
          (- code upper-case-a-code))
         ((and (>= code lower-case-a-code)
               (<= code lower-case-z-code))
          (- code -26 lower-case-a-code))
         ((and (>= code zero-code)
               (<= code nine-code))
          (- code -52 zero-code))
         ((= code (char-code #\+))
          62)
         ((= code (char-code #\/))
          63))))

(defun hex-string-p (string)
  "A hex-string is a string of only hex characters, 0-9, A-F, a-f"
  ;; TODO this is basically just an #'every
  (loop for char across string
        unless (typep char 'hex-char)
          do (return nil)
        finally (return t)))

(deftype hex-string ()
  "A hex-string is a string of only hex characters, 0-9, A-F, a-f"
  `(satisfies hex-string-p))

(defun b64-string-p (string)
  "A b64-string is a string of only b64 characters and padding, A-Z, a-z, 0-9, + / ="
  ;; TODO this is basically just an #'every
 (loop with length = (length string)
       for index from 0
       for char across string
       unless (or (typep char 'b64-char)
                  (and (> index (- length 3))
                       (typep char 'b64-pad)))
         do (return nil)
       finally (return t)))

(deftype b64-string ()
  "A b64-string is a string of only b64 characters and padding, A-Z, a-z, 0-9, + / ="
  `(satisfies b64-string-p))

(defun parse-hex (string)
  "Convert the hex string into a number."
  (assert (typep string 'hex-string))
  (loop with output = 0
        for char across string
        for position downfrom (* 4 (1- (length string))) by 4
        do (setf output
                 (dpb (char-hex char)
                      (byte 4 position)
                      output))
        finally (return output)))

(defun parse-b64 (string)
  "Convert the b64 string into a number."
  (assert (typep string 'b64-string))
  (loop with output = 0
        with pads = 0
        for char across string
        for position downfrom (* 6 (1- (length string))) by 6
        if (typep char 'b64-char)
          do (setf output
                   (dpb (char-b64 char)
                        (byte 6 position)
                        output))
        else do (incf pads)
        finally (return (ash output (* pads -8)))))


