---
title: "{{ replace .Name "-" " " | title }}"
date: {{ .Date | time.Format "2006-01-02" }}
publishDate: {{ .Date | time.Format "2006-01-02" }}
draft: true
---

