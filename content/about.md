---
title: "About"
date: 2022-07-22T13:09:13-04:00
publishDate: 2022-07-22
draft: false
---

I'm an amateur programmer and beginner cryptanalyst. This is my attempt to explain how the things I have written work, why I made the decisions I did, and what options I have forgone in the process.

For now the functions will be solving examples from <https://cryptopals.com> mostly because they are not restrictive on their licensing. I like that. I too am not restricitive on my licensing. Any code written here is technically AGPL3. I am not a lawyer but I understand it to mean you are free to use my code, so long as you give me credit. Consider the previous line to override anything in the license if it is contradictory.

You can expect to find error, mistakes, unaccounted for edge cases, and all sort of other issues with this code. I am no master wizard. Presenting this to the world is both exciting and nerve-wracking for me. I enjoy writing about my code but I am finding all sorts of issues with what I wrote as I go. Good luck. Code reviews and fixes are very much welcome - the source for this is on my GitLab.

I am very much open to discussion about my work; past, present, and future. Please contact me at <mailto:michael@mtreis86.com>
